import { Dps } from "./Class/Dps";
import { Heal } from "./Class/Heal";
import { Heroes } from "./Class/Heroes";
import { Slime } from "./Class/Slime";
import { Tonk } from "./Class/Tonk";

import TonkImg from "./Images/Tonk.png"
import DpsImg from "./Images/Dps.png"
import HealImg from "./Images/Heal.png"
import SlimeImg from "./Images/Slime.png"



const size = 5;
const grid: (Heroes | null)[][] = Array.from({ length: size }, () => Array(size).fill(null));
const entityClasses = [Tonk, Dps, Heal, Slime];

let selectedCell: any = null;
let selectedPiece: Heroes | null = null;
let piecePosition: { row: number | null; col: number | null; } = { row: null, col: null };
let possiblesMoves;
let cellValue;
let tempRow: number;
let tempCol: number;



/**
 * Recherche et renvoie les positions des entités (non nulles) dans la grille.
 * @param grid La grille de jeu contenant des entités.
 * @returns Un tableau d'objets { row: number; col: number } représentant les positions des entités.
 */
function findCharPositions(grid: (Heroes | null)[][]): { row: number; col: number }[] {
  const charPositions: { row: number; col: number }[] = [];
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid[row].length; col++) {
      if (grid[row][col] != null) {
        charPositions.push({ row, col });
      }
    }
  }
  return charPositions;
}

/**
 * Crée une grille HTML en fonction de la grille du jeu et des positions des objets Char.
 * @param grid La grille du jeu contenant des entités (Heroes | null).
 */
function createGrid(grid: (Heroes | null)[][]) {
  const table = document.createElement('table');
  const charPositions = findCharPositions(grid);

  for (let row = 0; row < grid.length; row++) {
    const tr = document.createElement('tr');

    for (let col = 0; col < grid[row].length; col++) {
      const td = document.createElement('td');

      td.id = `cell-${row}-${col}`;
      placeChar(charPositions, row, col, grid, td);
      td.addEventListener('click', function () {
        handleGridClick(row, col, grid);
      });

      tr.appendChild(td);
    }
    table.appendChild(tr);
  }

  const gridContainer = document.getElementById('grid-container');
  if (gridContainer) {
    gridContainer.appendChild(table);
  }
}

/**
 * Place une entité (Char) dans une cellule de la grille HTML en fonction de ses positions.
 * @param charPositions Les positions des entités (Char) dans la grille.
 * @param row La ligne de la cellule.
 * @param col La colonne de la cellule.
 * @param grid La grille du jeu contenant des entités.
 * @param td L'élément HTML de la cellule dans laquelle placer l'entité.
 */
function placeChar(charPositions: { row: number; col: number; }[], row: number, col: number, grid: (Heroes | null)[][], td: HTMLTableCellElement) {
  for (const charPosition of charPositions) {
    if (charPosition.row === row && charPosition.col === col) {
      const char = grid[charPosition.row][charPosition.col];
      td.textContent = char && char.name + " " + char.pv;

      const charImage = document.createElement("img");
      if (char) {
        charImage.src = char.image
        charImage.alt = char.name
        td.appendChild(charImage);
      }
    }
  }
}

/**
 * Gère l'événement de clic sur une cellule de la grille.
 * @param row La ligne de la cellule cliquée.
 * @param col La colonne de la cellule cliquée.
 * @param grid La grille du jeu contenant des entités.
 */
function handleGridClick(row: number, col: number, grid: (Heroes | null)[][]) {
  cellValue = grid[row][col];

  if (selectedPiece) {

    if (cellValue === null && isHighlighted(row, col, "red")) {
      if (piecePosition.row !== null && piecePosition.col !== null) {
        grid[piecePosition.row][piecePosition.col] = null;
      }
      grid[row][col] = selectedPiece;

      selectedPiece = null;
      selectedCell = null;
      possiblesMoves = null;

      resetCellBackgroundColors();
      updateGrid(grid);
      moveSlime()
    }

    else if (cellValue !== null && isHighlighted(row, col, "blue")) {
      let attack = selectedPiece.attack;
      let pv = cellValue.pv;
      let result = pv - attack
      cellValue.pv = result;

      selectedPiece = null;
      selectedCell = null;
      possiblesMoves = null;

      resetCellBackgroundColors();
      updateGrid(grid)
      moveSlime()
    }
  } else if (cellValue != null && cellValue.name != 'Slime') {

    selectedPiece = grid[row][col]
    const td = document.getElementById(`cell-${row}-${col}`);
    td && (td.style.backgroundColor = "red")

    tempRow = row
    tempCol = col

    let span = document.querySelector('#selectedPiece')
    span && selectedPiece && (span.innerHTML = selectedPiece.name)

  }
}


/**
 * Met à jour l'affichage de la grille en fonction de la grille du jeu et des positions des objets Char.
 * @param grid La grille du jeu contenant des entités (Heroes | null).
 */
function updateGrid(grid: (Heroes | null)[][]) {
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid[row].length; col++) {
      const td = document.getElementById(`cell-${row}-${col}`);
      if (grid[row][col] != null) {
        const char = grid[row][col];

        // Si 0 PV alors ON LE TUE !! 😈
        if (char && char.pv <= 0) {
          grid[row][col] = null;
          updateGrid(grid)
          return
        }

        td && (td.textContent = char && char.name + " " + char.pv);
        const charImage = document.createElement("img");
        if (char && td) {
          charImage.src = char.image
          charImage.alt = char.name
          td.appendChild(charImage);
        }
      } else {
        td && (td.textContent = null);
      }
    }
  }
}

/**
 * Vérifie si une cellule de la grille est mise en surbrillance avec une certaine couleur.
 * @param row La ligne de la cellule à vérifier.
 * @param col La colonne de la cellule à vérifier.
 * @param color La couleur de surbrillance à vérifier.
 * @returns True si la cellule est mise en surbrillance avec la couleur spécifiée, sinon False.
 */
function isHighlighted(row: number, col: number, color: string) {
  const td = document.getElementById(`cell-${row}-${col}`);
  return td && td.style.backgroundColor === color;
}

/**
 * Réinitialise la couleur de fond de toutes les cellules de la grille pour les rendre blanches.
 */
function resetCellBackgroundColors() {
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid[row].length; col++) {
      const td = document.getElementById(`cell-${row}-${col}`);
      if (td) {
        td.style.backgroundColor = "";
      }
    }
  }
}

/**
 * Recherche et renvoie les positions du Slime sur la grille.
 * @param grid La grille du jeu contenant des entités (Heroes | null).
 * @returns Un tableau d'objets { row: number, col: number } représentant les positions du Slime.
 */
function findSlimePosition(grid: (Heroes | null)[][]): { row: number; col: number }[] {
  const slimePosition: { row: number; col: number }[] = [];
  for (let row = 0; row < grid.length; row++) {
    for (let col = 0; col < grid[row].length; col++) {
      if (grid[row][col] != null && grid[row][col]?.name == 'Slime') {
        slimePosition.push({ row, col });
      }
    }
  }
  return slimePosition;
}

/**
 * Déplace le Slime vers la pièce ennemie la plus proche et l'attaque si elle est à proximité.
 */
function moveSlime() {
  const slimePosition = findSlimePosition(grid);
  const charPositions = findCharPositions(grid);

  let closestPiece = null;
  let closestDistance = Infinity;
  let closestPiecePosition = null;

  if (slimePosition[0]) {
    for (const charPosition of charPositions) {
      const piece = grid[charPosition.row][charPosition.col];
      const distance = Math.abs(slimePosition[0].row - charPosition.row) + Math.abs(slimePosition[0].col - charPosition.col);

      if (piece?.name !== 'Slime' && distance < closestDistance) {
        closestPiece = piece;
        closestDistance = distance;
        closestPiecePosition = charPosition;
      }
    }
  }

  if (closestPiecePosition) {

    const slimeRow = slimePosition[0].row;
    const slimeCol = slimePosition[0].col;

    const targetRow = closestPiecePosition.row;
    const targetCol = closestPiecePosition.col;

    const rowDiff = targetRow - slimeRow;
    const colDiff = targetCol - slimeCol;

    if (Math.abs(rowDiff) <= 1 && Math.abs(colDiff) <= 1) {
      grid[slimeRow][slimeCol]!.pv += grid[targetRow][targetCol]!.pv;
      grid[targetRow][targetCol] = grid[slimeRow][slimeCol]
      grid[slimeRow][slimeCol] = null
      updateGrid(grid)
      console.log("Attaquer la pièce la plus proche !");
    } else {
      if (Math.abs(rowDiff) > Math.abs(colDiff)) {
        if (rowDiff < 0) {
          grid[slimeRow - 1][slimeCol] = grid[slimeRow][slimeCol];
          grid[slimeRow][slimeCol] = null;
          updateGrid(grid)
          console.log("Déplacer le Slime vers le haut");
        } else {
          grid[slimeRow + 1][slimeCol] = grid[slimeRow][slimeCol];
          grid[slimeRow][slimeCol] = null;
          updateGrid(grid)
          console.log("Déplacer le Slime vers le bas");
        }
      } else {
        if (colDiff < 0) {
          grid[slimeRow][slimeCol - 1] = grid[slimeRow][slimeCol];
          grid[slimeRow][slimeCol] = null;
          updateGrid(grid)
          console.log("Déplacer le Slime vers la gauche");
        } else {
          grid[slimeRow][slimeCol + 1] = grid[slimeRow][slimeCol];
          grid[slimeRow][slimeCol] = null;
          updateGrid(grid)
          console.log("Déplacer le Slime vers la droite");
        }
      }
    }
  } else {
    console.log("Aucune pièce ennemie trouvée.");
  }
}

/**
 * Place aléatoirement des entités sur la grille.
 * @param grid La grille sur laquelle placer les entités.
 * @param entityClasses Un tableau de constructeurs d'entités.
 */
function placeEntitiesRandomly(grid: (Heroes | any)[][], entityClasses: (new (name: string, pv: number, attack: number, image: string) => Heroes)[]) {
  const numRows = grid.length;
  const numCols = grid[0].length;

  entityClasses.forEach(EntityClass => {
    let placed = false;
    while (!placed) {
      const randomRow = Math.floor(Math.random() * numRows);
      const randomCol = Math.floor(Math.random() * numCols);

      if (grid[randomRow][randomCol] === null || grid[randomRow][randomCol] === undefined) {
        let entity;

        if (EntityClass === Tonk) {
          entity = new EntityClass("Tonk", 50, 20, TonkImg);
        } else if (EntityClass === Dps) {
          entity = new EntityClass("Dps", 30, 10, DpsImg);
        } else if (EntityClass === Heal) {
          entity = new EntityClass("Heal", 20, 10, HealImg);
        } else if (EntityClass === Slime) {
          entity = new EntityClass("Slime", 20, 10, SlimeImg);
        }

        grid[randomRow][randomCol] = entity;
        placed = true;
      }
    }
  });
}

/**
 * Gestionnaire d'événements pour le bouton "Déplacer". Met en surbrillance les mouvements possibles
 * de la pièce sélectionnée en rouge sur la grille.
 */
const buttonMoves = document.querySelector('#move')
buttonMoves?.addEventListener('click', () => {
  if (selectedPiece) {
    resetCellBackgroundColors()

    const availableMoves = selectedPiece.move(tempRow, tempCol, grid)
    piecePosition = { row: tempRow, col: tempCol }

    for (const item of availableMoves) {
      const td = document.getElementById(`cell-${item.row}-${item.col}`);
      if (td) {
        td.style.backgroundColor = "red";
      }
    }
  }
})

/**
 * Gestionnaire d'événements pour le bouton "Action". Affiche les actions possibles
 * de la pièce sélectionnée en bleu sur la grille et affiche les dégâts de la pièce.
 */
const buttonAction = document.querySelector('#action')
buttonAction?.addEventListener('click', () => {
  if (selectedPiece) {
    resetCellBackgroundColors()

    let span = document.querySelector('#degats')
    span && selectedPiece && (span.innerHTML = String(selectedPiece.attack))

    const availableActions = selectedPiece.action(tempRow, tempCol, grid)
    piecePosition = { row: tempRow, col: tempCol }

    for (const item of availableActions) {
      const td = document.getElementById(`cell-${item.row}-${item.col}`);
      td && (td.style.backgroundColor = "blue");

    }
  }
})

placeEntitiesRandomly(grid, entityClasses);

createGrid(grid);
