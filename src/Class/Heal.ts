import { Heroes } from "./Heroes";

export class Heal extends Heroes {

    move(row: number, col: number, positions: (Heroes | null)[][]) {
        const possiblesMoves = [];

        possiblesMoves.push({ row: row - 1, col: col });
        possiblesMoves.push({ row: row, col: col + 1 });
        possiblesMoves.push({ row: row + 1, col: col });
        possiblesMoves.push({ row: row, col: col - 1 });

        possiblesMoves.push({ row: row + 1, col: col - 1 });
        possiblesMoves.push({ row: row + 1, col: col + 1 });
        possiblesMoves.push({ row: row - 1, col: col + 1 });
        possiblesMoves.push({ row: row - 1, col: col - 1 });

        return possiblesMoves;
    }

    action(row: number, col: number, positions: (Heroes | null)[][]): { row: number, col: number }[] {
        const possiblesActions = [];

        for (let i = row - 3; i <= row + 3; i++) {
            for (let j = col - 3; j <= col + 3; j++) {
                if (Math.abs(i - row) + Math.abs(j - col) >= 0 && Math.abs(i - row) + Math.abs(j - col) <= 3) {
                    if (i >= 0 && i < positions.length && j >= 0 && j < positions[i].length) {
                        possiblesActions.push({ row: i, col: j });
                    }
                }
            }
        }
        return possiblesActions;
    }

}