import { Heroes } from "./Heroes";


export class Slime extends Heroes {

    move(row: number, col: number, positions: (Heroes | null)[][]): { row: number; col: number; }[] {
        const possiblesActions = [];
        possiblesActions.push({ row: row - 1, col: col });
        possiblesActions.push({ row: row - 1, col: col - 1 });
        possiblesActions.push({ row: row, col: col + 1 });
        possiblesActions.push({ row: row - 1, col: col + 1 });
        possiblesActions.push({ row: row + 1, col: col });
        possiblesActions.push({ row: row + 1, col: col + 1 });
        possiblesActions.push({ row: row, col: col - 1 });
        possiblesActions.push({ row: row + 1, col: col - 1 });

        return possiblesActions;
        }


    action(row: number, col: number, positions: (Heroes | null)[][]): { row: number; col: number; }[] {
        const possiblesActions = [];
        possiblesActions.push({ row: row - 1, col: col });
        possiblesActions.push({ row: row - 1, col: col - 1 });
        possiblesActions.push({ row: row, col: col + 1 });
        possiblesActions.push({ row: row - 1, col: col + 1 });
        possiblesActions.push({ row: row + 1, col: col });
        possiblesActions.push({ row: row + 1, col: col + 1 });
        possiblesActions.push({ row: row, col: col - 1 });
        possiblesActions.push({ row: row + 1, col: col - 1 });

        return possiblesActions;
        }
    
}