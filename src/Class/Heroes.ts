
export abstract class Heroes {
    name: string;
    abstract move(row: number, col: number, positions: (Heroes | null)[][]) : {row : number , col :number }[]
    abstract action(row: number, col: number, positions: (Heroes | null)[][]) : {row : number , col :number }[]
    pv : number;
    attack : number;
    image : string;

    constructor(name: string,pv:number,attack:number,image:string) {
        this.name = name;
        this.pv = pv;
        this.attack = attack;
        this.image = image
    }
}