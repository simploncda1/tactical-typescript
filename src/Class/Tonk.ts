import { Heroes } from "./Heroes";

export class Tonk extends Heroes {

    move(row: number, col: number, positions: (Heroes | null)[][]): { row: number, col: number }[] {
        const possiblesMoves = [];

        possiblesMoves.push({ row: row - 1, col: col });
        possiblesMoves.push({ row: row, col: col + 1 });
        possiblesMoves.push({ row: row + 1, col: col });
        possiblesMoves.push({ row: row, col: col - 1 });

        const validMoves = possiblesMoves.filter(move => {
            const newRow = move.row;
            const newCol = move.col;

            return (
                newRow >= 0 &&
                newRow < positions.length &&
                newCol >= 0 &&
                newCol < positions[0].length &&
                positions[newRow][newCol] === null
            );
        });

        return validMoves;
    };

    action(row: number, col: number, positions: (Heroes | null)[][]): { row: number, col: number }[] {
        const possiblesActions = [];
        possiblesActions.push({ row: row - 1, col: col });
        possiblesActions.push({ row: row - 1, col: col - 1 });
        possiblesActions.push({ row: row, col: col + 1 });
        possiblesActions.push({ row: row - 1, col: col + 1 });
        possiblesActions.push({ row: row + 1, col: col });
        possiblesActions.push({ row: row + 1, col: col + 1 });
        possiblesActions.push({ row: row, col: col - 1 });
        possiblesActions.push({ row: row + 1, col: col - 1 });

        return possiblesActions;
    }

}